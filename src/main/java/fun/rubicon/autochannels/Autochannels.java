/*
 * RubiconBot - A open source Discord bot
 * Copyright (C) 2018  RubiconBot Dev Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package fun.rubicon.autochannels;

import fun.rubicon.autochannels.commands.AddAutochannelCommand;
import fun.rubicon.autochannels.commands.AutochannelCommand;
import fun.rubicon.autochannels.listener.ChannelListener;
import fun.rubicon.plugin.Plugin;

public class Autochannels extends Plugin {

    @Override
    public void init() {
        registerListener(new ChannelListener());

        AutochannelCommand autochannelCommand = new AutochannelCommand();
        autochannelCommand.registerSubCommand(new AddAutochannelCommand());
        registerCommand(autochannelCommand);
    }

    @Override
    public void onEnable() {
        super.onEnable();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }
}
